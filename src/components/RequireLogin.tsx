import { IonButton, IonText } from '@ionic/react'
import { Link } from 'react-router-dom'

export function RequireLogin() {
  return (
    <div className="ion-padding ion-text-center">
      <p>
        <IonText color="danger">
          This page is only available to registered users
        </IonText>
      </p>
      <Link to="/login">
        <IonButton>Login</IonButton>
      </Link>
    </div>
  )
}
export default RequireLogin
