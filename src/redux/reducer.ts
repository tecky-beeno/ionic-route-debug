import { RootAction } from './action'
export type RootState = {
  token: string | null
  loading_login: boolean
}

export function initState(): RootState {
  return {
    token: localStorage.getItem('token'),
    loading_login: false,
  }
}

export let rootReducer = (
  state: RootState = initState(),
  action: RootAction,
): RootState => {
  switch (action.type) {
    case 'patchRootState':
      return {
        ...state,
        ...action.state,
      }
    default:
      return state
  }
}
