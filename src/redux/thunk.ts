import { ThunkDispatch } from 'redux-thunk'
import { RootAction } from './action'
import { RootState } from './reducer'
import { RouterProps } from 'react-router-dom'

export type RootThunkDispatch = ThunkDispatch<RootState, null, RootAction>

export function loginThunk(history: RouterProps['history']) {
  return (dispatch: RootThunkDispatch) => {
    console.log('loading login ...')
    dispatch({ type: 'patchRootState', state: { loading_login: true } })
    setTimeout(() => {
      console.log('loaded login')
      const token = '123'
      localStorage.setItem('token', token)
      dispatch({
        type: 'patchRootState',
        state: { token, loading_login: false },
      })
      history.push('/home')
    }, 1000)
  }
}

export function logoutThunk(history: RouterProps['history']) {
  return (dispatch: RootThunkDispatch) => {
    localStorage.removeItem('token')
    dispatch({ type: 'patchRootState', state: { token: null } })
    history.push('/onboarding')
  }
}
