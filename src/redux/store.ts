import { rootReducer } from './reducer'
import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

export let rootEnhancer = compose(
  applyMiddleware(thunk),
  applyMiddleware(logger),
)

export let store = createStore(rootReducer, rootEnhancer)

export default store
