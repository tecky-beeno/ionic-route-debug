import { RootState } from './reducer'

export type RootAction = {
  type: 'patchRootState'
  state: Partial<RootState>
}
