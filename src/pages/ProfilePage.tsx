import { IonButton, IonContent } from '@ionic/react'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect, useHistory } from 'react-router-dom'
import IonPageFix from '../components/IonPageFix'
import { logoutThunk } from '../redux/thunk'
import { RootState } from '../redux/reducer'
import RequireLogin from '../components/RequireLogin'

export function ProfilePage() {
  const history = useHistory()
  const dispatch = useDispatch()
  const token = useSelector((state: RootState) => state.token)
  function logout() {
    dispatch(logoutThunk(history))
  }

  return (
    <IonPageFix path="/profile">
      <IonContent>
        <h1>Profile</h1>
        <IonButton onClick={logout}>Logout</IonButton>
      </IonContent>
    </IonPageFix>
  )
}
export default ProfilePage
