import { IonContent, IonButton } from '@ionic/react'
import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { RootState } from '../redux/reducer'
import RequireLogin from '../components/RequireLogin'
import IonPageFix from '../components/IonPageFix'

export function HomePage() {
  const token = useSelector((state: RootState) => state.token)
  return (
    <IonPageFix path="/home">
      {!token ? (
        <RequireLogin />
      ) : (
        <IonContent>
          <h1>Home</h1>
          <p>token: {token}</p>
          <Link to="/profile">
            <IonButton>Profile</IonButton>
          </Link>
        </IonContent>
      )}
    </IonPageFix>
  )
}
export default HomePage
