import { IonContent, IonButton, IonLoading, IonProgressBar } from '@ionic/react'
import React, { useState } from 'react'
import { loginThunk } from '../redux/thunk'
import { useDispatch, useSelector } from 'react-redux'
import { Link, RouterProps, useHistory } from 'react-router-dom'
import IonPageFix from '../components/IonPageFix'
import { RootState } from '../redux/reducer'

export function LoginPage() {
  const history = useHistory()
  const dispatch = useDispatch()
  const loading = useSelector((state: RootState) => state.loading_login)
  function login() {
    dispatch(loginThunk(history))
  }
  return (
    <IonPageFix path="/login">
      <IonContent>
        <h1>Login</h1>
        <IonButton onClick={login}>Login</IonButton>
        <Link to="/home">
          <IonButton>Force to home</IonButton>
        </Link>
        <Link to="/profile">
          <IonButton>Force to profile</IonButton>
        </Link>
        {loading ? <IonProgressBar type="indeterminate" /> : null}
      </IonContent>
    </IonPageFix>
  )
}
export default LoginPage
