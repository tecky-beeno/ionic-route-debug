import { IonContent, IonButton } from '@ionic/react'
import React from 'react'
import { Link } from 'react-router-dom'
import IonPageFix from '../components/IonPageFix'

export function OnBoardingPage() {
  return (
    <IonPageFix path="/onboarding">
      <IonContent>
        <h1>On Boarding</h1>
        <Link to="/login">
          <IonButton>Login</IonButton>
        </Link>
      </IonContent>
    </IonPageFix>
  )
}
export default OnBoardingPage
