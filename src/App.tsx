import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  useIonRouter,
} from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css'
import '@ionic/react/css/display.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/float-elements.css'
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css'
/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/typography.css'
import { ellipse, square, triangle } from 'ionicons/icons'
import React, { useLayoutEffect } from 'react'
/* Redux */
import { Provider, useSelector } from 'react-redux'
import { Redirect, Route, useHistory } from 'react-router-dom'
import HomePage from './pages/HomePage'
import LoginPage from './pages/LoginPage'
/* Pages */
import OnBoardingPage from './pages/OnBoardingPage'
import ProfilePage from './pages/ProfilePage'
import Tab2 from './pages/Tab2'
import Tab3 from './pages/Tab3'
import { RootState } from './redux/reducer'
import store from './redux/store'
/* Theme variables */
import './theme/variables.css'
import RequireLogin from './components/RequireLogin'
import { History } from 'history'

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <IonApp>
        <IonReactRouter>
          <Content />
        </IonReactRouter>
      </IonApp>
    </Provider>
  )
}

const PagesWithoutNavBar = [
  '/login',
  '/onboarding',
  '/register',
  '/admin/register',
]

function Content() {
  const router = useIonRouter()
  const pathname = router.routeInfo.pathname
  const hideNavBar = PagesWithoutNavBar.includes(pathname)
  const token = useSelector((state: RootState) => state.token)
  useLayoutEffect(() => {
    let e = document.querySelector('ion-tab-bar')
    if (!e) return
    e.style.display = hideNavBar ? 'none' : 'flex'
  }, [hideNavBar])
  return (
    <IonTabs>
      <IonRouterOutlet>
        <Route exact path="/">
          {
            // token ? <Redirect to="/home" /> : <Redirect to="/onboarding" />
            //  token ? <HomePage/> : <OnBoardingPage/>
            token ? <Redirect to="/home" /> : <Redirect to="/onboarding" />
          }
        </Route>

        <Route path="/onboarding" component={OnBoardingPage} />
        <Route path="/profile" component={ProfilePage} />
        <Route path="/home" component={HomePage} />
        <Route path="/login" component={LoginPage} />

        <Route path="/tab2">
          <Tab2 />
        </Route>
        <Route path="/tab3">
          <Tab3 />
        </Route>
      </IonRouterOutlet>

      <IonTabBar slot="bottom">
        <IonTabButton tab="tab1" href="/home">
          <IonIcon icon={triangle} />
          <IonLabel>Tab 1</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab2" href="/tab2">
          <IonIcon icon={ellipse} />
          <IonLabel>Tab 2</IonLabel>
        </IonTabButton>
        <IonTabButton tab="tab3" href="/tab3">
          <IonIcon icon={square} />
          <IonLabel>Tab 3</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  )
}

export default App
